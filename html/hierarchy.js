var hierarchy =
[
    [ "rs.ac.bg.etf.breilhelper.database.DatabaseHelper.Columns", "de/d9c/enumrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1database_1_1_database_helper_1_1_columns.html", null ],
    [ "rs.ac.bg.etf.breilhelper.frontend.ConnectionManager", "d1/d77/classrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1frontend_1_1_connection_manager.html", null ],
    [ "rs.ac.bg.etf.breilhelper.frontend.domain.Contact", "d6/dc5/classrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1frontend_1_1domain_1_1_contact.html", null ],
    [ "rs.ac.bg.etf.breilhelper.frontend.ContactInformationFetcher", "d1/d26/classrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1frontend_1_1_contact_information_fetcher.html", null ],
    [ "rs.ac.bg.etf.breilhelper.tracking.DynamicLocator", "d1/d1f/classrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1tracking_1_1_dynamic_locator.html", null ],
    [ "Exception", null, [
      [ "rs.ac.bg.etf.breilhelper.exceptions.DatabaseException", "d5/d03/classrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1exceptions_1_1_database_exception.html", null ],
      [ "rs.ac.bg.etf.breilhelper.exceptions.DoNotCacheException", "db/d7e/classrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1exceptions_1_1_do_not_cache_exception.html", null ],
      [ "rs.ac.bg.etf.breilhelper.exceptions.LetterNotRecognizedException", "de/d76/classrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1exceptions_1_1_letter_not_recognized_exception.html", null ]
    ] ],
    [ "rs.ac.bg.etf.breilhelper.frontend.MainActivity.InputType", "d6/deb/enumrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1frontend_1_1_main_activity_1_1_input_type.html", null ],
    [ "rs.ac.bg.etf.breilhelper.tasks.Language", "d8/d63/enumrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1tasks_1_1_language.html", null ],
    [ "rs.ac.bg.etf.breilhelper.frontend.LetterRecognizer.LetterReceiver", "dc/dae/interfacers_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1frontend_1_1_letter_recognizer_1_1_letter_receiver.html", [
      [ "rs.ac.bg.etf.breilhelper.frontend.MainActivity", "d7/d86/classrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1frontend_1_1_main_activity.html", null ],
      [ "rs.ac.bg.etf.breilhelper.frontend.WordRecognizer", "d6/de5/classrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1frontend_1_1_word_recognizer.html", null ],
      [ "rs.ac.bg.etf.breilhelper.keyboard.BrailInputService.LetterHandler", "dc/d1e/classrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1keyboard_1_1_brail_input_service_1_1_letter_handler.html", null ]
    ] ],
    [ "rs.ac.bg.etf.breilhelper.spellcheck.LevenshteinDistance", "dd/d56/classrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1spellcheck_1_1_levenshtein_distance.html", null ],
    [ "rs.ac.bg.etf.breilhelper.tracking.LocationMessage", "d3/d5b/classrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1tracking_1_1_location_message.html", null ],
    [ "rs.ac.bg.etf.breilhelper.tracking.DynamicLocator.LocationResult", "de/dfc/classrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1tracking_1_1_dynamic_locator_1_1_location_result.html", null ],
    [ "OnClickListener", null, [
      [ "rs.ac.bg.etf.breilhelper.frontend.BrailButtonListener", "dd/d20/classrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1frontend_1_1_brail_button_listener.html", null ],
      [ "rs.ac.bg.etf.breilhelper.keyboard.BrailButtonListener", "d4/d17/classrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1keyboard_1_1_brail_button_listener.html", null ]
    ] ],
    [ "rs.ac.bg.etf.breilhelper.database.SongCache", "d7/da3/classrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1database_1_1_song_cache.html", null ],
    [ "rs.ac.bg.etf.breilhelper.spellcheck.SpellCheckAssistant", "d4/dc6/classrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1spellcheck_1_1_spell_check_assistant.html", null ],
    [ "SpellCheckerSessionListener", null, [
      [ "rs.ac.bg.etf.breilhelper.frontend.AutoCorrectHelper", "d4/d06/classrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1frontend_1_1_auto_correct_helper.html", null ],
      [ "rs.ac.bg.etf.breilhelper.keyboard.BrailInputService", "d3/d1e/classrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1keyboard_1_1_brail_input_service.html", null ]
    ] ],
    [ "Thread", null, [
      [ "rs.ac.bg.etf.breilhelper.frontend.LetterRecognizer", "d9/dec/classrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1frontend_1_1_letter_recognizer.html", null ],
      [ "rs.ac.bg.etf.breilhelper.frontend.MainActivity.ReallyLongSpeaker", "da/d4f/classrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1frontend_1_1_main_activity_1_1_really_long_speaker.html", null ]
    ] ],
    [ "rs.ac.bg.etf.breilhelper.frontend.Toaster", "d7/d4e/classrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1frontend_1_1_toaster.html", null ],
    [ "Activity", null, [
      [ "rs.ac.bg.etf.breilhelper.frontend.MainActivity", "d7/d86/classrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1frontend_1_1_main_activity.html", null ]
    ] ],
    [ "AsyncTask", null, [
      [ "rs.ac.bg.etf.breilhelper.tasks.DownloadTask", "d5/d25/classrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1tasks_1_1_download_task.html", [
        [ "rs.ac.bg.etf.breilhelper.tasks.LongSpeaker", "d6/d72/classrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1tasks_1_1_long_speaker.html", null ],
        [ "rs.ac.bg.etf.breilhelper.tasks.NotCachingDownloadTask", "dd/d1f/classrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1tasks_1_1_not_caching_download_task.html", null ]
      ] ]
    ] ],
    [ "BroadcastReceiver", null, [
      [ "rs.ac.bg.etf.breilhelper.frontend.PhoneCallReceiver", "d4/d30/classrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1frontend_1_1_phone_call_receiver.html", [
        [ "rs.ac.bg.etf.breilhelper.frontend.CallReceiver", "d5/d53/classrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1frontend_1_1_call_receiver.html", null ]
      ] ],
      [ "rs.ac.bg.etf.breilhelper.frontend.SmsReceiver", "de/de7/classrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1frontend_1_1_sms_receiver.html", null ]
    ] ],
    [ "InputMethodService", null, [
      [ "rs.ac.bg.etf.breilhelper.keyboard.BrailInputService", "d3/d1e/classrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1keyboard_1_1_brail_input_service.html", null ]
    ] ],
    [ "LocationListener", null, [
      [ "rs.ac.bg.etf.breilhelper.tracking.GPSTracker", "d7/dc9/classrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1tracking_1_1_g_p_s_tracker.html", null ]
    ] ],
    [ "Service", null, [
      [ "rs.ac.bg.etf.breilhelper.tracking.GPSTracker", "d7/dc9/classrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1tracking_1_1_g_p_s_tracker.html", null ]
    ] ],
    [ "SQLiteOpenHelper", null, [
      [ "rs.ac.bg.etf.breilhelper.database.DatabaseHelper", "de/d59/classrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1database_1_1_database_helper.html", null ]
    ] ],
    [ "TimerTask", null, [
      [ "rs.ac.bg.etf.breilhelper.tracking.DynamicLocator.GetLastLocation", "da/d59/classrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1tracking_1_1_dynamic_locator_1_1_get_last_location.html", null ]
    ] ]
];