var dir_0334387f5b193d46e19ad0d9bae1cd4c =
[
    [ "domain", "dir_6fbab6a5ddcbd4e5ff7acc2f2592f5e9.html", "dir_6fbab6a5ddcbd4e5ff7acc2f2592f5e9" ],
    [ "AutoCorrectHelper.java", "da/d01/_auto_correct_helper_8java_source.html", null ],
    [ "BrailButtonListener.java", "d8/d5f/frontend_2_brail_button_listener_8java_source.html", null ],
    [ "CallReceiver.java", "d4/d82/_call_receiver_8java_source.html", null ],
    [ "ConnectionManager.java", "d2/dc7/_connection_manager_8java_source.html", null ],
    [ "ContactInformationFetcher.java", "dd/d4b/_contact_information_fetcher_8java_source.html", null ],
    [ "LetterRecognizer.java", "d3/d95/_letter_recognizer_8java_source.html", null ],
    [ "MainActivity.java", "d4/d79/_main_activity_8java_source.html", null ],
    [ "PhoneCallReceiver.java", "d5/dbd/_phone_call_receiver_8java_source.html", null ],
    [ "SmsReceiver.java", "d8/d49/_sms_receiver_8java_source.html", null ],
    [ "Toaster.java", "de/d7f/_toaster_8java_source.html", null ],
    [ "WordRecognizer.java", "de/df8/_word_recognizer_8java_source.html", null ]
];