var classrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1frontend_1_1_connection_manager =
[
    [ "ConnectionManager", "d1/d77/classrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1frontend_1_1_connection_manager.html#a1e40d895b2e12c32d2777cada9049715", null ],
    [ "getFileFromRawResource", "d1/d77/classrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1frontend_1_1_connection_manager.html#ae8ed78936868ae328fe25deb1c0182b7", null ],
    [ "isNetAvailable", "d1/d77/classrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1frontend_1_1_connection_manager.html#a4b726057beefb705245c7c957c97df27", null ],
    [ "sayWarningNoInternet", "d1/d77/classrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1frontend_1_1_connection_manager.html#aa42e50e347d2f20789896e5a79299764", null ],
    [ "connected", "d1/d77/classrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1frontend_1_1_connection_manager.html#afd725defaf9cc4129060057dd258b0b2", null ],
    [ "context", "d1/d77/classrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1frontend_1_1_connection_manager.html#a9986d18916484c60db0466aef3cb57a0", null ],
    [ "haveConnectedMobile", "d1/d77/classrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1frontend_1_1_connection_manager.html#abe86a1213dfd7aa856f7fe1f06bf02be", null ],
    [ "haveConnectedWifi", "d1/d77/classrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1frontend_1_1_connection_manager.html#a7f40db7d34744905f6bdcc9d872c046f", null ]
];