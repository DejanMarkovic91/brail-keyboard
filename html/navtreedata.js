var NAVTREE =
[
  [ "Brail keyboard", "index.html", [
    [ "Klase", null, [
      [ "Spisak klasa", "annotated.html", "annotated" ],
      [ "Spisak klasa", "classes.html", null ],
      [ "Hijerarhija klasa", "hierarchy.html", "hierarchy" ],
      [ "Svi članovi klasa", "functions.html", [
        [ "Sve", "functions.html", null ],
        [ "Funkcije", "functions_func.html", null ],
        [ "Promenljive", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Datoteke", null, [
      [ "Spisak datoteka", "files.html", "files" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
".html",
"d7/da3/classrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1database_1_1_song_cache.html#a0fb5f28ea5d6e9a0a589260612df5a37"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';