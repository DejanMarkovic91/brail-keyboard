var classrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1database_1_1_database_helper =
[
    [ "Columns", "de/d9c/enumrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1database_1_1_database_helper_1_1_columns.html", "de/d9c/enumrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1database_1_1_database_helper_1_1_columns" ],
    [ "DatabaseHelper", "de/d59/classrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1database_1_1_database_helper.html#ade6f9859eb5ec07076ee29d5e93a3b38", null ],
    [ "clearDatabase", "de/d59/classrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1database_1_1_database_helper.html#a805077a514f99d045d6edf9973bbbbee", null ],
    [ "getSong", "de/d59/classrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1database_1_1_database_helper.html#a7fdc75795f5f69f4e0754218057134ec", null ],
    [ "insert", "de/d59/classrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1database_1_1_database_helper.html#ac79c8032e1b62bbd13825cddfd3bb954", null ],
    [ "onCreate", "de/d59/classrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1database_1_1_database_helper.html#a8b27c010ae50ed5197a2dcbfa7bc15e6", null ],
    [ "onUpgrade", "de/d59/classrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1database_1_1_database_helper.html#a10a182a9d207f78062942edf172ad0a9", null ],
    [ "DATABASE_NAME", "de/d59/classrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1database_1_1_database_helper.html#a849b3a97b32cb444c7ca242d0d1d09d4", null ],
    [ "VERSION", "de/d59/classrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1database_1_1_database_helper.html#a9ebb8f777b3b15e0304cb12796db92d0", null ]
];