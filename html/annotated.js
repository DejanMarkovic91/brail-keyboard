var annotated =
[
    [ "rs", null, [
      [ "ac", null, [
        [ "bg", null, [
          [ "etf", null, [
            [ "breilhelper", null, [
              [ "database", null, [
                [ "DatabaseHelper", "de/d59/classrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1database_1_1_database_helper.html", "de/d59/classrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1database_1_1_database_helper" ],
                [ "SongCache", "d7/da3/classrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1database_1_1_song_cache.html", "d7/da3/classrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1database_1_1_song_cache" ]
              ] ],
              [ "exceptions", null, [
                [ "DatabaseException", "d5/d03/classrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1exceptions_1_1_database_exception.html", "d5/d03/classrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1exceptions_1_1_database_exception" ],
                [ "DoNotCacheException", "db/d7e/classrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1exceptions_1_1_do_not_cache_exception.html", "db/d7e/classrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1exceptions_1_1_do_not_cache_exception" ],
                [ "LetterNotRecognizedException", "de/d76/classrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1exceptions_1_1_letter_not_recognized_exception.html", "de/d76/classrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1exceptions_1_1_letter_not_recognized_exception" ]
              ] ],
              [ "frontend", null, [
                [ "domain", null, [
                  [ "Contact", "d6/dc5/classrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1frontend_1_1domain_1_1_contact.html", "d6/dc5/classrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1frontend_1_1domain_1_1_contact" ]
                ] ],
                [ "AutoCorrectHelper", "d4/d06/classrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1frontend_1_1_auto_correct_helper.html", "d4/d06/classrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1frontend_1_1_auto_correct_helper" ],
                [ "BrailButtonListener", "dd/d20/classrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1frontend_1_1_brail_button_listener.html", "dd/d20/classrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1frontend_1_1_brail_button_listener" ],
                [ "CallReceiver", "d5/d53/classrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1frontend_1_1_call_receiver.html", "d5/d53/classrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1frontend_1_1_call_receiver" ],
                [ "ConnectionManager", "d1/d77/classrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1frontend_1_1_connection_manager.html", "d1/d77/classrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1frontend_1_1_connection_manager" ],
                [ "ContactInformationFetcher", "d1/d26/classrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1frontend_1_1_contact_information_fetcher.html", "d1/d26/classrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1frontend_1_1_contact_information_fetcher" ],
                [ "LetterRecognizer", "d9/dec/classrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1frontend_1_1_letter_recognizer.html", "d9/dec/classrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1frontend_1_1_letter_recognizer" ],
                [ "MainActivity", "d7/d86/classrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1frontend_1_1_main_activity.html", "d7/d86/classrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1frontend_1_1_main_activity" ],
                [ "PhoneCallReceiver", "d4/d30/classrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1frontend_1_1_phone_call_receiver.html", "d4/d30/classrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1frontend_1_1_phone_call_receiver" ],
                [ "SmsReceiver", "de/de7/classrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1frontend_1_1_sms_receiver.html", "de/de7/classrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1frontend_1_1_sms_receiver" ],
                [ "Toaster", "d7/d4e/classrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1frontend_1_1_toaster.html", "d7/d4e/classrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1frontend_1_1_toaster" ],
                [ "WordRecognizer", "d6/de5/classrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1frontend_1_1_word_recognizer.html", "d6/de5/classrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1frontend_1_1_word_recognizer" ]
              ] ],
              [ "keyboard", null, [
                [ "BrailButtonListener", "d4/d17/classrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1keyboard_1_1_brail_button_listener.html", "d4/d17/classrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1keyboard_1_1_brail_button_listener" ],
                [ "BrailInputService", "d3/d1e/classrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1keyboard_1_1_brail_input_service.html", "d3/d1e/classrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1keyboard_1_1_brail_input_service" ]
              ] ],
              [ "spellcheck", null, [
                [ "LevenshteinDistance", "dd/d56/classrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1spellcheck_1_1_levenshtein_distance.html", "dd/d56/classrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1spellcheck_1_1_levenshtein_distance" ],
                [ "SpellCheckAssistant", "d4/dc6/classrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1spellcheck_1_1_spell_check_assistant.html", "d4/dc6/classrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1spellcheck_1_1_spell_check_assistant" ]
              ] ],
              [ "tasks", null, [
                [ "DownloadTask", "d5/d25/classrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1tasks_1_1_download_task.html", "d5/d25/classrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1tasks_1_1_download_task" ],
                [ "Language", "d8/d63/enumrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1tasks_1_1_language.html", "d8/d63/enumrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1tasks_1_1_language" ],
                [ "LongSpeaker", "d6/d72/classrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1tasks_1_1_long_speaker.html", "d6/d72/classrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1tasks_1_1_long_speaker" ],
                [ "NotCachingDownloadTask", "dd/d1f/classrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1tasks_1_1_not_caching_download_task.html", "dd/d1f/classrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1tasks_1_1_not_caching_download_task" ]
              ] ],
              [ "tracking", null, [
                [ "DynamicLocator", "d1/d1f/classrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1tracking_1_1_dynamic_locator.html", "d1/d1f/classrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1tracking_1_1_dynamic_locator" ],
                [ "GPSTracker", "d7/dc9/classrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1tracking_1_1_g_p_s_tracker.html", "d7/dc9/classrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1tracking_1_1_g_p_s_tracker" ],
                [ "LocationMessage", "d3/d5b/classrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1tracking_1_1_location_message.html", "d3/d5b/classrs_1_1ac_1_1bg_1_1etf_1_1breilhelper_1_1tracking_1_1_location_message" ]
              ] ]
            ] ]
          ] ]
        ] ]
      ] ]
    ] ]
];