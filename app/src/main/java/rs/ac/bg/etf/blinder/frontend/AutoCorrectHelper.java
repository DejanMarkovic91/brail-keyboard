package rs.ac.bg.etf.blinder.frontend;

import android.content.Context;
import android.util.Log;
import android.view.textservice.SentenceSuggestionsInfo;
import android.view.textservice.SpellCheckerSession;
import android.view.textservice.SuggestionsInfo;
import android.view.textservice.TextInfo;
import android.view.textservice.TextServicesManager;

import java.util.Locale;

/**
 * Check input and show suggestion if possible.
 *
 * @author Dejan Markovic
 */
public class AutoCorrectHelper implements SpellCheckerSession.SpellCheckerSessionListener {

    private MainActivity activity;
    private SpellCheckerSession mScs;
    private TextServicesManager mTsm;
    private String currentWord = "";

    public AutoCorrectHelper(MainActivity activity) {
        this.activity = activity;

        mTsm = (TextServicesManager) activity.getSystemService(Context.TEXT_SERVICES_MANAGER_SERVICE);
        mScs = mTsm.newSpellCheckerSession(null, Locale.ENGLISH, this, false);
    }

    public void getSuggestion(String word) {
        currentWord = word;
        if (word == null || word.equals("")) return;
        if (mScs != null)
            mScs.getSuggestions(new TextInfo(word), 1);
        else
            activity.wordFound(word);
    }

    /**
     * Check syntax.
     *
     * @param results holder objekat za rezultate provere
     */
    @Override
    public void onGetSuggestions(SuggestionsInfo[] results) {
        try {
            if (results[0].getSuggestionsAttributes() != 5)
                currentWord = results[0].getSuggestionAt(0);
            Log.i(AutoCorrectHelper.class.getName(), "Autocorrect returned: " + currentWord);
        } catch (Exception e) {
            e.printStackTrace();
        }
        activity.wordFound(currentWord);
    }

    @Override
    public void onGetSentenceSuggestions(SentenceSuggestionsInfo[] sentenceSuggestionsInfos) {
        //not used for sentences
    }

    public void closeServiceConnection() {
        if (mScs != null) {//release resources if any are allocated
            mScs.close();
        }
    }
}
