package rs.ac.bg.etf.blinder.keyboard;

import android.view.View;

/**
 * Button listener za input method servis.
 *
 * @author Dejan Markovic
 */
public class BrailButtonListener implements View.OnClickListener {

    private BrailInputService service;
    private int index;

    public BrailButtonListener(BrailInputService service, int i) {
        this.service = service;
        this.index = i;
    }


    @Override
    public void onClick(View v) {
        new Thread() {

            @Override
            public void run() {
                service.letterRecognizer.setDot(index);
            }

        }.start();
    }
}
