package rs.ac.bg.etf.blinder.frontend;

import android.util.Log;

import java.util.ArrayList;

import rs.ac.bg.etf.blinder.exceptions.LetterNotRecognizedException;

/**
 * Cuva podatke koje je sve button-e kliknuo korisnik
 *
 * @author Dejan Markovic
 */
public class LetterRecognizer extends Thread {

    public static String getLanguage() {
        return language;
    }

    private static String language;

    public static String GENERATE_WORD_OUTPUT_URL = "http://api.voicerss.org/?key=7202fbc271b14ce99ceb903d46261eea&hl=en-us&r=-2&src=";

    private static final long TIME_BETWEEN_DOTS = 300; //ms
    private static final long REFRESH_PERIOD = 500; //ms

    private short dots = 0;
    private boolean working = true;
    private boolean stateChanged = false;
    private static LetterRecognizer letter;

    private ArrayList<LetterReceiver> receivers = new ArrayList<>();
    private int retryCounter;

    private LetterRecognizer() {
        super("Letter");
        language = "en";
        /*if (language == null) {
            switch (Locale.getDefault().getLanguage()) {
                case "sr":
                    language = Language.sr.name();
                    break;
                case "de":
                    language = Language.de.name();
                    break;
                case "es":
                    language = Language.es.name();
                    break;
                default:
                    language = "en";
                    break;
            }
            GENERATE_WORD_OUTPUT_URL = "http://translate.google.com/translate_tts?tl=" + language + "&q=";
            Log.i(LetterRecognizer.class.getName(), "Language changed to " + language);
        }*/
        start();
    }

    /**
     * Singleton
     *
     * @return instance
     */
    public static LetterRecognizer getLetterRecognizer() {
        if (letter == null || letter.isInterrupted() || !letter.isAlive()) {
            letter = new LetterRecognizer();
        }
        return letter;
    }

    /**
     * Multitouch listeners will call this method and set specific dot in Brail letter to true.
     *
     * @param index
     */
    public synchronized void setDot(int index) {
        dots |= (1 << index);
        stateChanged = true;
        Log.i(LetterRecognizer.class.getName(), "Letter: " + dots);
    }

    @Override
    public void run() {
        try {
            while (working) {
                Thread.sleep(REFRESH_PERIOD);
                synchronized (this) {
                    if (stateChanged) {
                        stateChanged = false;
                        continue;
                    }
                    if (tryRecognizeLetter()) {
                        //recognized, now we must wait timeout
                        wait(TIME_BETWEEN_DOTS);
                        //someone typed something
                        System.out.println("Changed: " + stateChanged);
                        if (stateChanged) continue;//maybe other letter
                        else {
                            //we recognized letter
                            char letter;
                            appendLetterToReceivers(letter = recognizeFromDots());
                            dots = 0;
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Convert pressed dots on screen into valid letter
     *
     * @return letter if it is recognized from dots
     * @throws LetterNotRecognizedException if no letter has been recognized
     */
    private char recognizeFromDots() throws LetterNotRecognizedException {
        stateChanged = false;
        switch (dots) {
            case 0x01:
                return 'a';
            case 0x05:
                return 'b';
            case 0x03:
                return 'c';
            case 0xb:
                return 'd';
            case 0x09:
                return 'e';
            case 0x07:
                return 'f';
            case 0x0f:
                return 'g';
            case 0x0d:
                return 'h';
            case 0x06:
                return 'i';
            case 0x0e:
                return 'j';
            case 0x11:
                return 'k';
            case 0x15:
                return 'l';
            case 0x13:
                return 'm';
            case 0x1b:
                return 'n';
            case 0x19:
                return 'o';
            case 0x17:
                return 'p';
            case 0x1f:
                return 'q';
            case 0x1d:
                return 'r';
            case 0x16:
                return 's';
            case 0x1e:
                return 't';
            case 0x31:
                return 'u';
            case 0x35:
                return 'v';
            case 0x2e:
                return 'w';
            case 0x33:
                return 'x';
            case 0x3b:
                return 'y';
            case 0x39:
                return 'z';
            case 0x1c:
                return '!';
            case 0x10:
                return '\'';
            case 0x04:
                return ',';
            case 0x30:
                return '-';
            case 0x2c:
                return '.';
            case 0x34:
                return '?';
            case 0x20:
                return '#';//CAPS
            case 0x3a:
                return '$';//NUMBER 1,2,3,4,5,6,7,8,9,0
        }
        if (language.equals("es")) {
            switch (dots) {
                case 0x3d:
                    return 'á';
                case 0x36:
                    return 'é';
                case 0x3e:
                    return 'ú';
                case 0x2f:
                    return 'ñ';
                case 0x2d:
                    return 'ü';
                case 0x12:
                    return 'í';
                case 0x32:
                    return 'ó';
            }
        }
        if (language.equals("de")) {
            switch (dots) {
                case 0x36:
                    return 'ß';
                case 0x2d:
                    return 'ü';
                case 0x26:
                    return 'ö';
                case 0x1a:
                    return 'ä';
            }
        }
        if (language.equals("sr")) {
            switch (dots) {
                case 0x35:
                    return 'ć';
                case 0x33:
                    return 'č';
                case 0x43:
                    return 'đ';
                case 0x41:
                    return 'š';
                case 0x54:
                    return 'ž';
            }
        }
        dots = 0;
        throw new LetterNotRecognizedException(dots);//pogresno slovo
    }

    /**
     * Tries to recognize letter.
     *
     * @return boolean flag containing info if we can recognize any valid letter
     */
    public boolean tryRecognizeLetter() {
        try {
            recognizeFromDots();
            return true;
        } catch (LetterNotRecognizedException e) {
            stateChanged = false;
            return false;
        }
    }

    /**
     * If dot has been pressed in mean time from recognition, this method is called.
     *
     * @return flag
     */
    public synchronized boolean stateChanged() {
        return stateChanged;
    }

    /**
     * Stops thread from listening on dots.
     */
    public void stopWorking() {
        if (receivers.isEmpty()) {
            working = false;
            interrupt();
        }
    }

    /**
     * Inserts space into word.
     */
    public synchronized void insertSpace() {
        appendLetterToReceivers(' ');
    }

    /**
     * Inserts special symbol in word so character before this one will be removed when word is parsed.
     */
    public synchronized void removeOneLetter() {
        appendLetterToReceivers('<');
    }

    public void appendLetterToReceivers(char c) {
        for (LetterReceiver r : receivers) {
            try {
                r.appendLetter(c);
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        }
    }

    public void addReceiver(LetterReceiver r) {
        if (receivers.contains(r)) return;
        receivers.add(r);
    }

    public void removeReceiver(LetterReceiver r) {
        receivers.remove(r);
    }

    public interface LetterReceiver {

        public void appendLetter(char c);

    }
}
