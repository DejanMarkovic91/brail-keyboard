package rs.ac.bg.etf.blinder.tasks;

import android.content.Context;
import android.media.MediaPlayer;

/**
 * Poseban proces koji skida i izgovara audio fajl ali ga ne kesira u bazi.
 *
 * @author Dejan Markovic
 */
public class NotCachingDownloadTask extends DownloadTask {

    public NotCachingDownloadTask(Context context) {
        super(context);
    }

    @Override
    protected void onPostExecute(Void result) {
        try {
            MediaPlayer mediaPlayer = new MediaPlayer();
            mediaPlayer.setDataSource(file.getPath());
            mediaPlayer.prepare();
            mediaPlayer.start();
            file.delete();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
