package rs.ac.bg.etf.blinder.frontend;

import android.content.Context;

import java.util.Date;


/**
 * Klasa koja je zaduzena da zaustavi bilo kakav audio izlaz ako dodje poziv na telefon.
 * (Prvenstveno se misli na long speaking sesiju) Po zavrsetku poziva audio sesija se restartuje!
 *
 * @author Dejan Markovic
 */
public class CallReceiver extends PhoneCallReceiver {

    private MainActivity activity;

    private MainActivity getActivity() {
        if (activity == null) {
            activity = MainActivity.getInstance();
        }
        return activity;
    }

    @Override
    protected void onIncomingCallStarted(Context ctx, String number, Date start) {
        if (getActivity() == null) return;
        getActivity().pauseLongSpeaker();
    }

    @Override
    protected void onOutgoingCallStarted(Context ctx, String number, Date start) {
        if (getActivity() == null) return;
        getActivity().pauseLongSpeaker();
    }

    @Override
    protected void onIncomingCallEnded(Context ctx, String number, Date start, Date end) {
        if (getActivity() == null) return;
        getActivity().restartLongSpeaker();
    }

    @Override
    protected void onOutgoingCallEnded(Context ctx, String number, Date start, Date end) {
        if (getActivity() == null) return;
        getActivity().restartLongSpeaker();
    }

}
