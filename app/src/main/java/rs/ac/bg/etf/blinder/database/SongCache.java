package rs.ac.bg.etf.blinder.database;

import android.content.Context;
import android.media.MediaPlayer;

import java.io.File;

import rs.ac.bg.etf.blinder.tasks.DownloadTask;
import rs.ac.bg.etf.blinder.tasks.NotCachingDownloadTask;
import rs.ac.bg.etf.blinder.exceptions.DatabaseException;
import rs.ac.bg.etf.blinder.exceptions.DoNotCacheException;

/**
 * Interface for easy database access.
 *
 * @author Dejan Markovic
 */
public class SongCache {

    private Context context;
    private DatabaseHelper helper;
    private static SongCache instance;

    public SongCache(Context context) {
        this.context = context;
        this.helper = new DatabaseHelper(context);
        instance = this;
    }

    /**
     * Lazy instanciranje za input method
     *
     * @param context Kontekst za koji se instancira
     * @return SongCache instanca
     */
    public static SongCache getInstance(Context context) {
        if (instance == null) {
            instance = new SongCache(context);
        }
        return instance;
    }

    /**
     * Pokusava da pronadje pesmu u bazi podataka, i ako uspe odmah je pusta kroz audio api.
     * Ukoliko je ne nadje pokrece se podsistem za skidanje pesme, ista se kesira u skladu sa flegom
     * koji je drugi argumrnt metode, a tek nakon toga se pusta audio fajl
     *
     * @param name        Ime audio fajla koji pokusavamo da pustimo
     * @param shouldCache Da li zelimo da se ovaj audio fajl kesira (necemo ga kesirati ako je on veliki)
     */
    public void tryFindOrDownload(String name, boolean shouldCache) {
        try {
            if (!shouldCache) throw new DoNotCacheException();
            File file = helper.getSong(name);
            if (file == null) throw new DatabaseException("Error reading file from database!");
            playSong(file);
            file.delete();
        } catch (Exception e) {
            //if cache miss get from online database (but check caching)
            if (!shouldCache) new NotCachingDownloadTask(context).execute(name, null, null);
            else new DownloadTask(context).execute(name, null, null);
        }
    }

    /**
     * Sprovodi kesiranje pesme u bazi.
     *
     * @param song
     */
    public void cacheSong(File song) {
        try {
            helper.insert(song);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sprovodi invalidaciju kesa i ciscenje baze podataka.
     */
    public void invalidateCache() {
        helper.clearDatabase();
    }

    /**
     * Pusta audio fajl koji prosledjen kao argument ovoj metodi
     *
     * @param file audio fajl u internoj memoriji
     */
    private void playSong(File file) {
        try {
            MediaPlayer mediaPlayer = new MediaPlayer();
            mediaPlayer.setDataSource(file.getAbsolutePath());
            mediaPlayer.prepare();
            mediaPlayer.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
