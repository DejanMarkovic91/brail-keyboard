package rs.ac.bg.etf.blinder.frontend;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;

import rs.ac.bg.etf.blinder.frontend.domain.Contact;

/**
 * Prima broadcast od operativnog sistema sa sms porukama i izgovara svaku.
 *
 * @author Dejan Markovic
 */
public class SmsReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Bundle bundle = intent.getExtras();
        if (bundle != null) {
            if (MainActivity.getInstance() == null) return;
            Object[] pdusObj = (Object[]) bundle.get("pdus");
            SmsMessage currentMessage = SmsMessage.createFromPdu((byte[]) pdusObj[0]);

            //fill contact with data
            Contact contact = new Contact();
            contact.setNumber(currentMessage.getDisplayOriginatingAddress());
            contact.setMessage(currentMessage.getDisplayMessageBody());

            if (MainActivity.getInstance().getContactName(contact)) {
                Log.e(SmsReceiver.class.getName(), "Sms received: " + contact);
                //success finding name in contacts on phone
                StringBuilder build = new StringBuilder();
                build.append(contact.getName()).append(" sent you message: ").append(contact.getMessage());
                MainActivity.getInstance().startLongSpeakingSession(build.toString().replaceAll(" ", "%20"));
            } else {
                Log.e(SmsReceiver.class.getName(), "Sms received: " + contact);
                StringBuilder build = new StringBuilder();
                build.append(" You received message from number ").append(contact.getNumber()).append(":").append(contact.getMessage());
                MainActivity.getInstance().startLongSpeakingSession(build.toString().replaceAll(" ", "%20"));
            }
        }
    }
}
