package rs.ac.bg.etf.blinder.tasks;

import android.content.Context;
import android.media.MediaPlayer;

import java.io.FileInputStream;

/**
 * Proces koji obavlja izgovaranja dugacke recenice.
 *
 * @author Dejan Markovic
 */
public class LongSpeaker extends DownloadTask {

    private MediaPlayer mediaPlayer;

    public LongSpeaker(Context context) {
        super(context);
    }

    @Override
    protected void onPostExecute(Void result) {
        try {
            FileInputStream fis = new FileInputStream(file);
            mediaPlayer = new MediaPlayer();
            mediaPlayer.setDataSource(fis.getFD());
            mediaPlayer.prepare();
            mediaPlayer.start();
            file.delete();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void stopSpeaking() {
        //stop media player
        if (mediaPlayer != null && mediaPlayer.isPlaying()) {
            mediaPlayer.pause();
        }
    }

    public void restartSpeaking() {
        try {
            mediaPlayer.seekTo(0);
            mediaPlayer.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
