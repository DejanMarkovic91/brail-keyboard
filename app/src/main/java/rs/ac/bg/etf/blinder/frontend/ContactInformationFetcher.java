package rs.ac.bg.etf.blinder.frontend;

import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;

import rs.ac.bg.etf.blinder.frontend.domain.Contact;

/**
 * Radi upit u androidovu bazu podataka o kontaktima i dostavlja nama neophodne podatke.
 *
 * @author Dejan Markovic
 */
public class ContactInformationFetcher {

    private MainActivity activity;

    public ContactInformationFetcher(MainActivity activity) {
        this.activity = activity;
    }

    public boolean resolveContact(Contact con) {

        String number = con.getNumber();

        //check invalid numbers
        if (number == null) return false;
        if (number.length() < 5) return false;

        //get with content resolver
        ContentResolver localContentResolver = activity.getContentResolver();
        Cursor contactLookupCursor =
                localContentResolver.query(
                        Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI,
                                Uri.encode(number)),
                        new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME, ContactsContract.PhoneLookup._ID},
                        null,
                        null,
                        null);
        try {
            while (contactLookupCursor.moveToNext()) {
                String contactName = contactLookupCursor.getString(contactLookupCursor.getColumnIndexOrThrow(ContactsContract.PhoneLookup.DISPLAY_NAME));
                String contactId = contactLookupCursor.getString(contactLookupCursor.getColumnIndexOrThrow(ContactsContract.PhoneLookup._ID));
                con.setName(contactName);
                con.setId(contactId);
                return true;
            }
        } finally {
            contactLookupCursor.close();
        }
        return false;
    }

}
