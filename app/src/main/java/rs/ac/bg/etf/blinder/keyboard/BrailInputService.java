package rs.ac.bg.etf.blinder.keyboard;

import android.content.Context;
import android.graphics.Color;
import android.inputmethodservice.InputMethodService;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Vibrator;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputConnection;
import android.view.textservice.SentenceSuggestionsInfo;
import android.view.textservice.SpellCheckerSession;
import android.view.textservice.SuggestionsInfo;
import android.view.textservice.TextInfo;
import android.view.textservice.TextServicesManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Locale;

import rs.ac.bg.etf.blinder.R;
import rs.ac.bg.etf.blinder.database.SongCache;
import rs.ac.bg.etf.blinder.exceptions.LetterNotRecognizedException;
import rs.ac.bg.etf.blinder.frontend.LetterRecognizer;
import rs.ac.bg.etf.blinder.tasks.LongSpeaker;

/**
 * Input method servis koji se pojavljuje kada korisnik kuca poruke na regularan nacin.
 * (Naravno pre toga je potrebno da korisnik dozvoli ovom metodu u opcijama da se koristi)
 *
 * @author Dejan Markovic
 */
public class BrailInputService extends InputMethodService implements SpellCheckerSession.SpellCheckerSessionListener {

    LetterRecognizer letterRecognizer;

    private SpellCheckerSession mScs;
    private TextServicesManager mTsm;

    private static BrailInputService instance;

    public static BrailInputService getInstance() {
        return instance;
    }

    private Vibrator vibrator;
    private LongSpeaker longSpeaker;
    private LetterHandler handler;

    private String currentWord = "";

    private boolean caps;
    private boolean number;

    private Button one;
    private Button two;
    private Button three;
    private Button four;
    private Button five;
    private Button six;

    @Override
    public void onBindInput() {
        fetchAll();
    }

    @Override
    public void onUnbindInput() {
        if (letterRecognizer != null) letterRecognizer.removeReceiver(handler);
    }

    private void fetchAll() {
        letterRecognizer = LetterRecognizer.getLetterRecognizer();
        if (!letterRecognizer.isAlive()) letterRecognizer.start();
        letterRecognizer.addReceiver(handler);

        mTsm = (TextServicesManager) getSystemService(Context.TEXT_SERVICES_MANAGER_SERVICE);
        Locale locale = Locale.ENGLISH;
        switch (letterRecognizer.getLanguage()) {
            case "sr":
                locale = Locale.ENGLISH;
                break;
            case "de":
                locale = Locale.GERMAN;
                break;
            case "es":
                locale = Locale.ENGLISH;
                break;
        }
        mScs = mTsm.newSpellCheckerSession(null, locale, this, false);
    }

    @Override
    public View onCreateInputView() {
        View mainView = getLayoutInflater().inflate(R.layout.keyboard_layout, null);

        instance = this;
        longSpeaker = new LongSpeaker(this);
        Button power1 = (Button) mainView.findViewById(R.id.power_button1);
        Button power2 = (Button) mainView.findViewById(R.id.power_button2);
        one = (Button) mainView.findViewById(R.id.one);
        two = (Button) mainView.findViewById(R.id.two);
        three = (Button) mainView.findViewById(R.id.three);
        four = (Button) mainView.findViewById(R.id.four);
        five = (Button) mainView.findViewById(R.id.five);
        six = (Button) mainView.findViewById(R.id.six);

        one.setOnClickListener(new BrailButtonListener(this, 0));
        two.setOnClickListener(new BrailButtonListener(this, 1));
        three.setOnClickListener(new BrailButtonListener(this, 2));
        four.setOnClickListener(new BrailButtonListener(this, 3));
        five.setOnClickListener(new BrailButtonListener(this, 4));
        six.setOnClickListener(new BrailButtonListener(this, 5));

        handler = new LetterHandler();

        vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);

        power1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                letterRecognizer.removeOneLetter();
            }
        });

        power2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                letterRecognizer.insertSpace();
            }
        });
        fetchAll();
        return mainView;

    }

    public void getSuggestion(String word) {
        currentWord = word;
        if (mScs != null && word != null && word.length() > 0)
            mScs.getSuggestions(new TextInfo(word), 1);
    }

    @Override
    public void onGetSuggestions(SuggestionsInfo[] results) {
        try {
            if (results[0].getSuggestionsAttributes() != 5)
                System.out.println(results[0].getSuggestionAt(0));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void vibrate(long length) {
        vibrator.vibrate(length);
    }

    @Override
    public void onGetSentenceSuggestions(SentenceSuggestionsInfo[] sentenceSuggestionsInfos) {

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        letterRecognizer.stopWorking();
    }


    private class LetterHandler implements LetterRecognizer.LetterReceiver {

        private char getNumber(char c) throws LetterNotRecognizedException {
            switch (c) {
                case 'a':
                    return '1';
                case 'b':
                    return '2';
                case 'c':
                    return '3';
                case 'd':
                    return '4';
                case 'e':
                    return '5';
                case 'f':
                    return '6';
                case 'g':
                    return '7';
                case 'h':
                    return '8';
                case 'i':
                    return '9';
                case 'j':
                    return '0';
            }
            throw new LetterNotRecognizedException(c);
        }

        public void showToast(final char letters, final long length, final int color) {
            new HandlerThread("Toaster") {
                @Override
                public void onLooperPrepared() {
                    final Toast toast = new Toast(BrailInputService.this);
                    LayoutInflater inflater = (LayoutInflater) BrailInputService.this.
                            getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                    TextView layout = (TextView) inflater.inflate(R.layout.letter_toast, null);
                    layout.setTextColor(color);
                    layout.setText(Character.toString(letters));
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(layout);
                    toast.show();

                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            toast.cancel();
                        }
                    }, length);
                }

            }.start();
        }


        @Override
        public void appendLetter(char c) {
            vibrate(100);
            SongCache cache = SongCache.getInstance(BrailInputService.this);
            InputConnection ic = getCurrentInputConnection();
            if (c == ' ') {
                //try autocorrect word
                if (!currentWord.equals("")) {
                    longSpeaker = new LongSpeaker(BrailInputService.this);
                    longSpeaker.execute(currentWord);
                }
                getSuggestion(currentWord);
                currentWord = "";
                ic.commitText(" ", 1);
                showToast('_', 1000, Color.RED);
            } else if (c == '<') {//delete one letter
                if (currentWord.length() >= 2) {
                    currentWord = currentWord.substring(0, currentWord.length() - 2);
                }
                ic.deleteSurroundingText(1, 0);
                showToast('<', 1000, Color.RED);
            } else {
                if (c == '#') {
                    caps = !caps;
                    cache.tryFindOrDownload("big%20" + (caps ? "start" : "end"), true);
                } else if (c == '$') {
                    number = !number;
                    cache.tryFindOrDownload("number%20" + (number ? "start" : "end"), true);
                } else {
                    try {
                        char ch = (number ? getNumber(c) : c);
                        cache.tryFindOrDownload(ch + "", true);
                        if (caps) {
                            ch = Character.toUpperCase(c);
                            currentWord = currentWord + ch;
                            ic.commitText(ch + "", 1);
                            showToast(ch, 1000, Color.RED);
                        } else {
                            currentWord = currentWord + c;
                            ic.commitText(c + "", 1);
                            showToast(c, 1000, Color.RED);
                        }
                    } catch (LetterNotRecognizedException e) {
                        longSpeaker = new LongSpeaker(BrailInputService.this);
                        longSpeaker.execute("Input character is not a number!");
                        e.printStackTrace();
                        return;
                    }
                }
            }
        }


    }
}
