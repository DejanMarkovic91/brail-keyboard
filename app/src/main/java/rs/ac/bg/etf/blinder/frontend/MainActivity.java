package rs.ac.bg.etf.blinder.frontend;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.telephony.PhoneNumberUtils;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.ByteArrayOutputStream;
import java.lang.reflect.Method;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.LinkedList;
import java.util.List;

import rs.ac.bg.etf.blinder.R;
import rs.ac.bg.etf.blinder.database.SongCache;
import rs.ac.bg.etf.blinder.exceptions.LetterNotRecognizedException;
import rs.ac.bg.etf.blinder.frontend.domain.Contact;
import rs.ac.bg.etf.blinder.tasks.LongSpeaker;
import rs.ac.bg.etf.blinder.tracking.DynamicLocator;
import rs.ac.bg.etf.blinder.tracking.GPSTracker;
import rs.ac.bg.etf.blinder.tracking.LocationMessage;

/**
 * Main activity, responsible for whole graphic design.
 *
 * @author Dejan Markovic
 */
public class MainActivity extends Activity implements LetterRecognizer.LetterReceiver {

    public static String[] options;

    public static final long SHORT_VIBRATE = 100;
    public static final long MEDIUM_VIBRATE = 200;
    public static final long LONG_VIBRATE = 500;

    private Button power1;
    private Button power2;

    private Button one;
    private Button two;
    private Button three;
    private Button four;
    private Button five;
    private Button six;

    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;

    LetterRecognizer letterRecognizer;

    private SongCache cache;
    private ConnectionManager manager;
    private AutoCorrectHelper checker;
    private static MainActivity instance;
    private Vibrator vibrator;

    private StringBuilder message = new StringBuilder();
    private String number = "";
    private InputType inputType = InputType.TEXT;
    private LongSpeaker longSpeaker;
    private ContactInformationFetcher contactFetcher;
    private SmsManager smsManager = SmsManager.getDefault();
    private WordRecognizer wordBuffer;
    private static boolean spekedOut = false;
    private ReallyLongSpeaker rs;
    private TextView box;
    private int regime = 0;//0 regular, 1 - sms, 2 - call

    private GPSTracker gps;

    public static MainActivity getInstance() {
        return instance;
    }

    /**
     * Postavljanje layouta i dohvatanje referenci komponenti
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        options = new String[5];
        options[0] = getString(R.string.cancel);
        options[1] = getString(R.string.read_message);
        options[2] = getString(R.string.send_message);
        options[3] = getString(R.string.call);
        options[4] = getString(R.string.sos_message);

        instance = this;
        cache = new SongCache(this);
        manager = new ConnectionManager(this);
        manager.isNetAvailable();
        checker = new AutoCorrectHelper(this);
        power1 = (Button) findViewById(R.id.power_button1);
        power2 = (Button) findViewById(R.id.power_button2);
        one = (Button) findViewById(R.id.one);
        two = (Button) findViewById(R.id.two);
        three = (Button) findViewById(R.id.three);
        four = (Button) findViewById(R.id.four);
        five = (Button) findViewById(R.id.five);
        six = (Button) findViewById(R.id.six);

        box = (TextView) findViewById(R.id.box);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);

        //set drawer width
        int width = getResources().getDisplayMetrics().widthPixels;
        DrawerLayout.LayoutParams params = (android.support.v4.widget.DrawerLayout.LayoutParams) mDrawerList.getLayoutParams();
        params.width = width;
        mDrawerList.setLayoutParams(params);

        mDrawerList.setAdapter(new ArrayAdapter<String>(this, R.layout.big_list_item, options));

        vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        longSpeaker = new LongSpeaker(this);
        contactFetcher = new ContactInformationFetcher(this);

        addListeners();
        mDrawerLayout.setDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {

            }

            @Override
            public void onDrawerOpened(View drawerView) {
                /*if (!spekedOut) {
                    startLongSpeakingSession(MainActivity.this.getString(R.string.first_part_options_menu).replaceAll(" ", "%20"),
                            MainActivity.this.getString(R.string.second_part_options_menu).replaceAll(" ", "%20"));
                    spekedOut = true;
                }*/
            }

            @Override
            public void onDrawerClosed(View drawerView) {

            }

            @Override
            public void onDrawerStateChanged(int newState) {

            }
        });

        gps = new GPSTracker(this);
        if (!gps.canGetLocation()) {
            gps.showSettingsAlert();
        }
    }

    /**
     * Add listeners on buttons.
     */
    private void addListeners() {
        one.setOnClickListener(new BrailButtonListener(this, 0));
        two.setOnClickListener(new BrailButtonListener(this, 1));
        three.setOnClickListener(new BrailButtonListener(this, 2));
        four.setOnClickListener(new BrailButtonListener(this, 3));
        five.setOnClickListener(new BrailButtonListener(this, 4));
        six.setOnClickListener(new BrailButtonListener(this, 5));

        power1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                letterRecognizer.removeOneLetter();
            }
        });

        power2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                letterRecognizer.insertSpace();
            }
        });

        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String msg = "";
                switch (position) {
                    case 1://read
                        msg = message.toString();
                        Log.i(MainActivity.class.getName(), "Sentence: " + msg);
                        startLongSpeakingSession(msg.equals("") ? getString(
                                R.string.no_input_text).replaceAll(" ", "%20") :
                                msg.replaceAll(" ", "%20"));
                        break;
                    case 2://send
                        msg = message.toString();
                        if (msg.equals("")) msg = box.getText().toString();
                        if (inputType.equals(InputType.TEXT) || regime != 1) {
                            //ask for number
                            regime = 1;
                            box.setText("");
                            if (msg.equals("")) {
                                startLongSpeakingSession(getString(R.string.no_input_text).replaceAll(" ", "%20"));
                                break;
                            }
                            inputType = InputType.NUMBER;
                            wordBuffer.blockInput();
                            startLongSpeakingSession(getString(R.string.now_type_in_number).replaceAll(" ", "%20"));
                        } else {
                            //send sms
                            if (number.equals("")) number = box.getText().toString();
                            Log.e(MainActivity.class.getName(), "Sms to number: " + number);
                            if (validatePhoneNumber(number)) {
                                try {
                                    smsManager.sendTextMessage(number, null, msg, null, null);
                                    sendSms(MainActivity.this, number, msg);
                                    wordBuffer.unblockInput();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            } else {
                                msg = getString(R.string.number) + " " + number + getString(R.string.is_wrong_try_again);
                                startLongSpeakingSession(msg.replaceAll(" ", "%20"));
                                inputType = InputType.NUMBER;
                            }
                        }
                        break;
                    case 0:
                        //cancel
                        if (regime == 1) {
                            startLongSpeakingSession(getString(R.string.message_canceled).replaceAll(" ", "%20"));
                        } else if (regime == 2) {
                            startLongSpeakingSession(getString(R.string.call_canceled).replaceAll(" ", "%20"));
                        }
                        wordBuffer.unblockInput();
                        regime = 0;
                        number = "";
                        box.setText("");
                        inputType = InputType.TEXT;
                        break;
                    case 3:
                        //call
                        if (number.equals("")) number = box.getText().toString();
                        if (inputType == InputType.TEXT || regime != 2) {
                            regime = 2;
                            box.setText("");
                            inputType = InputType.NUMBER;
                            wordBuffer.blockInput();
                            startLongSpeakingSession(getString(R.string.now_type_in_number).replaceAll(" ", "%20"));
                            break;
                        }
                        Log.i(MainActivity.class.getName(), "Input message will be parsed as number. Number: " + number);
                        if (validatePhoneNumber(number)) {
                            Intent intent = new Intent(Intent.ACTION_CALL);
                            intent.setData(Uri.parse("tel:" + number));
                            startActivity(intent);
                            inputType = InputType.TEXT;
                            wordBuffer.unblockInput();
                        } else {
                            startLongSpeakingSession(MainActivity.this.getString(R.string.wrong_number).replaceAll(" ", "%20"));
                            inputType = InputType.NUMBER;
                        }
                        break;
                    case 4://sos
                        Location location = gps.getLocation();
                        if (location != null) {
                            LocationMessage message = new LocationMessage();
                            message.setLongitude(location.getLongitude());
                            message.setLatitude(location.getLatitude());
                            message.setAltitude(location.getAltitude());
                            message.setAccuracy(new Double(location.getAccuracy()));
                            message.setTime(location.getTime());
                            message.setUser(getUsername());
                            ObjectMapper mapper = new ObjectMapper();
                            Log.i(MainActivity.class.getName(), "Sos message: " + message.toString());
                            try {
                                //TODO replace with commented line when in production
                                //smsManager.sendTextMessage("phoneNo", null, mapper.writeValueAsString(message), null, null);
                                sendSms(MainActivity.this, "0631234567", mapper.writeValueAsString(message));
                                startLongSpeakingSession(MainActivity.this.getString(R.string.help_is_on_the_way).replaceAll(" ", "%20"));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            //u suprotnom pokusacemo dodatno da sacekamo
                            DynamicLocator.LocationResult locationResult = new DynamicLocator.LocationResult() {
                                @Override
                                public void gotLocation(Location location) {
                                    if (location == null) {
                                        startLongSpeakingSession(MainActivity.this.getString(R.string.no_location_available).replaceAll(" ", "%20"));
                                        return;
                                    }
                                    LocationMessage message = new LocationMessage();
                                    message.setLongitude(location.getLongitude());
                                    message.setLatitude(location.getLatitude());
                                    message.setAltitude(location.getAltitude());
                                    message.setAccuracy(new Double(location.getAccuracy()));
                                    message.setTime(location.getTime());
                                    message.setUser(getUsername());
                                    ObjectMapper mapper = new ObjectMapper();
                                    Log.i(MainActivity.class.getName(), "Sos message: " + message.toString());
                                    try {
                                        //TODO replace with commented line when in production
                                        //smsManager.sendTextMessage("phoneNo", null, mapper.writeValueAsString(message), null, null);
                                        //sendSms(MainActivity.this, "0631234567", mapper.writeValueAsString(message));
                                        startLongSpeakingSession(MainActivity.this.getString(R.string.help_is_on_the_way).replaceAll(" ", "%20"));
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            };
                            DynamicLocator loc = new DynamicLocator();
                            loc.getLocation(MainActivity.this, locationResult);
                            Log.i(MainActivity.class.getName(), "Immediate loocation is not available!");
                            //startLongSpeakingSession("LOCATION NOT AVAILABLE!".replaceAll(" ", "%20"));
                        }
                        break;
                }
                if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
                    mDrawerLayout.closeDrawer(GravityCompat.START);
                }
            }
        });
    }

    /**
     * Check validity of input number.
     *
     * @param number number to be checked
     * @return boolean containing info about validity of number
     */
    private boolean validatePhoneNumber(String number) {
        if (PhoneNumberUtils.isGlobalPhoneNumber(number) ||
                PhoneNumberUtils.isEmergencyNumber(number))
            return true;
        else return false;
    }

    /**
     * Spellcheck and speak word.
     *
     * @param word rec koja je prepoznata
     */
    public void setCurrentWord(String word) {
        checker.getSuggestion(word.replaceAll("<", ""));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        gps.stopUsingGPS();
        letterRecognizer.stopWorking();
    }

    public SongCache getcache() {
        return cache;
    }

    /**
     * Called when word is recognized.
     *
     * @param word
     */
    public void wordFound(String word) {
        cache.tryFindOrDownload(word, true); // download and cache
        vibrate(MEDIUM_VIBRATE);// heptic feedback
        if (inputType.equals(InputType.TEXT) && regime == 0)
            message.append(word).append(" ");
        else {
            number = word;
            /*if (validatePhoneNumber(number)) {
                try {
                    //TODO replace with commented line when in production
                    //smsManager.sendTextMessage("phoneNo", null, mapper.writeValueAsString(message), null, null);
                    sendSms(MainActivity.this, "0637403089", message.toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                startLongSpeakingSession("Number " + number + " is wrong, please try again!");
                inputType = InputType.NUMBER;
            }*/
        }
    }

    /**
     * Vibrirati telefon za odgovarajuci broj milisekundi
     *
     * @param length vreme u milisekundama
     */
    public void vibrate(long length) {
        vibrator.vibrate(length);
    }

    @Override
    public void onStart() {
        super.onStart();
        letterRecognizer = LetterRecognizer.getLetterRecognizer();
        if (!letterRecognizer.isAlive()) letterRecognizer.start();
        wordBuffer = WordRecognizer.getWordRecognizer();
        letterRecognizer.addReceiver(this);
        letterRecognizer.addReceiver(wordBuffer);
        checker = new AutoCorrectHelper(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        longSpeaker.stopSpeaking();
        checker.closeServiceConnection();
        letterRecognizer.removeReceiver(wordBuffer);
        letterRecognizer.removeReceiver(this);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_MENU) {
            new Handler().postDelayed(drawerOpener, 200);
            return true;
        } else if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
                mDrawerLayout.closeDrawer(GravityCompat.START);
                return true;
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    /**
     * Runnable that handles opening drawer with animation
     */
    private Runnable drawerOpener = new Runnable() {

        @Override
        public void run() {
            mDrawerLayout.openDrawer(Gravity.LEFT);
        }
    };

    /**
     * Pauzira trenutnu sesiju pustanja audio snimka
     */
    public void pauseLongSpeaker() {
        if (rs != null) rs.interrupt();
        if (longSpeaker != null) longSpeaker.stopSpeaking();
    }

    /**
     * Restartuje audio snimak
     */
    public void restartLongSpeaker() {
        longSpeaker.restartSpeaking();
    }

    /**
     * Dohvata ime kontakt
     *
     * @param contact Prosledimo informacije koje znamo kroz holder
     * @return dobijamo boolean informaciju o uspesnosti poziva
     */
    public boolean getContactName(Contact contact) {
        return contactFetcher.resolveContact(contact);
    }

    //TEST

    /**
     * Send sms from this phone to itself, without using provider.
     *
     * @param context activity in which it is launched
     * @param sender  number of sender
     * @param body    text of message
     * @throws Exception in case of error along the way
     */
    private void sendSms(Context context, String sender, String body) throws Exception {
        Log.i(MainActivity.class.getName(), "Sending message to " + sender + " with text: " + body);
        byte[] pdu = null;
        byte[] scBytes = PhoneNumberUtils.networkPortionToCalledPartyBCD("0000000000");
        byte[] senderBytes = PhoneNumberUtils.networkPortionToCalledPartyBCD(sender);
        int lsmcs = scBytes.length;
        byte[] dateBytes = new byte[7];
        Calendar calendar = new GregorianCalendar();
        dateBytes[0] = reverseByte((byte) (calendar.get(Calendar.YEAR)));
        dateBytes[1] = reverseByte((byte) (calendar.get(Calendar.MONTH) + 1));
        dateBytes[2] = reverseByte((byte) (calendar.get(Calendar.DAY_OF_MONTH)));
        dateBytes[3] = reverseByte((byte) (calendar.get(Calendar.HOUR_OF_DAY)));
        dateBytes[4] = reverseByte((byte) (calendar.get(Calendar.MINUTE)));
        dateBytes[5] = reverseByte((byte) (calendar.get(Calendar.SECOND)));
        dateBytes[6] = reverseByte((byte) ((calendar.get(Calendar.ZONE_OFFSET) +
                calendar.get(Calendar.DST_OFFSET)) / (60 * 1000 * 15)));

        ByteArrayOutputStream bo = new ByteArrayOutputStream();
        bo.write(lsmcs);
        bo.write(scBytes);
        bo.write(0x04);
        bo.write((byte) sender.length());
        bo.write(senderBytes);
        bo.write(0x00);
        bo.write(0x00);
        bo.write(dateBytes);

        String sReflectedClassName = "com.android.internal.telephony.GsmAlphabet";
        Class cReflectedNFCExtras = Class.forName(sReflectedClassName);
        Method stringToGsm7BitPacked = cReflectedNFCExtras.getMethod("stringToGsm7BitPacked", new Class[]{String.class});
        stringToGsm7BitPacked.setAccessible(true);
        byte[] bodybytes = (byte[]) stringToGsm7BitPacked.invoke(null, body);
        bo.write(bodybytes);
        pdu = bo.toByteArray();

        // broadcast the SMS_RECEIVED to registered receivers
        broadcastSmsReceived(context, pdu);

        // or, directly send the message into the inbox and let the usual SMS handling happen - SMS appearing in Inbox, a notification with sound, etc.
        startSmsReceiverService(context, pdu);

        Log.i(MainActivity.class.getName(), "Message sent!");
    }

    private void broadcastSmsReceived(Context context, byte[] pdu) {
        Intent intent = new Intent();
        intent.setAction("android.provider.Telephony.SMS_RECEIVED");
        intent.putExtra("pdus", new Object[]{pdu});
        context.sendBroadcast(intent);
    }

    private void startSmsReceiverService(Context context, byte[] pdu) {
        Intent intent = new Intent();
        intent.setClassName("com.android.mms", "com.android.mms.transaction.SmsReceiverService");
        intent.setAction("android.provider.Telephony.SMS_RECEIVED");
        intent.putExtra("pdus", new Object[]{pdu});
        intent.putExtra("format", "3gpp");
        context.startService(intent);
    }

    private byte reverseByte(byte b) {
        return (byte) ((b & 0xF0) >> 4 | (b & 0x0F) << 4);
    }

    /**
     * Zaustavlja tekuci audio playback ako postoji i pokrece novi sa zadatim tekstom.
     *
     * @param s
     */
    public void startLongSpeakingSession(String s) {
        pauseLongSpeaker();
        longSpeaker = new LongSpeaker(this);
        longSpeaker.execute(s, null, null);
    }

    /**
     * Za odredjeni broj karaktera rest nece da primi jedan poziv vec moramo sukcesivno uraditi dva
     *
     * @param s1 prvi string
     * @param s2 drugi string
     */
    public void startLongSpeakingSession(final String s1, final String s2) {
        rs = new ReallyLongSpeaker();
        rs.start();
    }

    /**
     * Dohvata username is Google Play accounta korisnika sa telefona.
     *
     * @return username ili null ako korisnik nije ulogovan sa Google Play store nalogom na telefonu
     */
    public String getUsername() {
        AccountManager manager = AccountManager.get(this);
        Account[] accounts = manager.getAccountsByType("com.google");
        List<String> possibleEmails = new LinkedList<String>();

        for (Account account : accounts) {
            // TODO: Check possibleEmail against an email regex or treat
            // account.name as an email address only for certain account.type values.
            possibleEmails.add(account.name);
        }

        if (!possibleEmails.isEmpty() && possibleEmails.get(0) != null) {
            String email = possibleEmails.get(0);
            String[] parts = email.split("@");

            if (parts.length > 1)
                return parts[0];
        }
        return null;
    }

    private boolean capsState = false;
    private boolean numberState = false;

    @Override
    public void appendLetter(char c) {
        System.out.println("Char appended to box: " + c);
        String data = box.getText().toString();
        if (c == ' ') {
            // word grouped
            data = "";
            capsState = false;
            numberState = false;
        } else if (c == '<') {//delete one letter
            if (data.length() > 0)
                data = data.substring(0, data.length() - 1);
        } else {
            if (c == '#') {
                capsState = !capsState;
            } else if (c == '$') {
                numberState = !numberState;
            } else {
                try {
                    data += (numberState ? getNumber(c) : capsState ? Character.toUpperCase(c) : c) + "";
                } catch (LetterNotRecognizedException e) {

                }
            }
        }
        final String d = data;
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                box.setText(d);
            }
        });
    }

    private char getNumber(char c) throws LetterNotRecognizedException {
        switch (c) {
            case 'a':
                return '1';
            case 'b':
                return '2';
            case 'c':
                return '3';
            case 'd':
                return '4';
            case 'e':
                return '5';
            case 'f':
                return '6';
            case 'g':
                return '7';
            case 'h':
                return '8';
            case 'i':
                return '9';
            case 'j':
                return '0';
        }
        throw new LetterNotRecognizedException(c);
    }

    /**
     * Input type.
     */
    private enum InputType {
        TEXT,
        NUMBER
    }

    private class ReallyLongSpeaker extends Thread {
        @Override
        public void run() {
            longSpeaker = new LongSpeaker(MainActivity.this);
            longSpeaker.execute(MainActivity.this.getString(R.string.first_part_options_menu).replaceAll(" ", "%20"), null, null);
            try {
                Thread.sleep(7000);
                longSpeaker = new LongSpeaker(MainActivity.this);
                longSpeaker.execute(MainActivity.this.getString(R.string.second_part_options_menu).replaceAll(" ", "%20"), null, null);
            } catch (Exception e) {
            }
        }
    }
}
