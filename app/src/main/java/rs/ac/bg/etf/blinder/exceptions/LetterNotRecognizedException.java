package rs.ac.bg.etf.blinder.exceptions;

/**
 * Baca se u slucaju da nije prepoznata sekvenca pritisnutih dugmica na ekranu.
 *
 * @author Dejan Markovic
 */
public class LetterNotRecognizedException extends Exception {

    public LetterNotRecognizedException(int mask) {
        super("FATAL ERROR: Letter not recognized from mask = " + mask);
    }
}
