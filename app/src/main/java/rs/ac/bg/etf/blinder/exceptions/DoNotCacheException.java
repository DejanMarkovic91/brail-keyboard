package rs.ac.bg.etf.blinder.exceptions;

/**
 * Baca se u slucaju da ne zelimo da kesiramo audio fajl.
 *
 * @author Dejan Markovic
 */
public class DoNotCacheException extends Exception {

    public DoNotCacheException() {
        super("INFO: We are not caching this song.");
    }
}
