package rs.ac.bg.etf.blinder.exceptions;

/**
 * Baca se u slucaju da se dogodi greska prilikom komunikacije sa bazom podataka.
 *
 * @author Dejan Markovic
 */
public class DatabaseException extends Exception {

    public DatabaseException(String detailMessage) {
        super(detailMessage);
    }
}
