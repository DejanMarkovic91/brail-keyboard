package rs.ac.bg.etf.blinder.tasks;

import android.content.Context;
import android.media.MediaPlayer;
import android.os.AsyncTask;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import rs.ac.bg.etf.blinder.database.SongCache;
import rs.ac.bg.etf.blinder.frontend.LetterRecognizer;

/**
 * Skida audio fajl sa izgovorom prosledjene reci/slova.
 *
 * @author Dejan Markovic
 */
public class DownloadTask extends AsyncTask<String, Void, Void> {

    protected Context context;
    protected File file;

    public DownloadTask(Context context) {
        this.context = context;
    }

    @Override
    protected Void doInBackground(String... word) {
        InputStream input = null;
        OutputStream output = null;
        HttpURLConnection connection = null;
        try {
            URL url = new URL(LetterRecognizer.GENERATE_WORD_OUTPUT_URL + word[0]);
            connection = (HttpURLConnection) url.openConnection();
            connection.connect();

            // expect HTTP 200 OK, so we don't mistakenly save error report
            // instead of the file
            if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                file = null;
                throw new Exception("[RESPONSE: " +
                        connection.getResponseCode() + "] Data: " + word[0]);
            }

            // this will be useful to display download percentage
            // might be -1: server did not report the length

            // download the file
            input = connection.getInputStream();
            //file = new File(word + ".waw");
            file = new File(context.getFilesDir(), word[0] + ".waw");
            output = new FileOutputStream(file);
            byte data[] = new byte[4096];
            int count;
            while ((count = input.read(data)) != -1) {
                // allow canceling with back button
                if (isCancelled()) {
                    input.close();
                    return null;
                }
                output.write(data, 0, count);
            }
            output.flush();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (output != null)
                    output.close();
                if (input != null)
                    input.close();
            } catch (IOException ignored) {
            }

            if (connection != null)
                connection.disconnect();
        }
        return null;
    }

    /**
     * Izgovara skinutu rec
     *
     * @param result not used
     */
    @Override
    protected void onPostExecute(Void result) {
        try {
            MediaPlayer mediaPlayer = new MediaPlayer();
            mediaPlayer.setDataSource(file.getPath());
            mediaPlayer.prepare();
            mediaPlayer.start();
            SongCache.getInstance(context).cacheSong(file);
            file.delete();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
